const endpoint = 'http://localhost:5984';
const database = 'assignment';

var fs = require("fs");
var path = require("path");
var nano = require('nano')(endpoint);

var p = __dirname + '/../data';
var db = nano.db.use(database);

// Indexes single json document
function indexDocument(f) {
  var arr = f.split('/')
  var type = arr[arr.length - 1]
  var id = arr[arr.length - 2]
  fs.readFile(f, 'utf8', function(err, data) {
    if (err) {
      throw err;
    }
    var doc = JSON.parse(data)
    doc['Id'] = id
    doc['Type'] = type
    db.insert(doc, function(err, body) {
      if (err) {
        console.log('[db.insert] ', err.message);
        return;
      }
    });
  });
}

// Clean up the database we created previously
nano.db.destroy(database, function() {
  // Create a new database
  nano.db.create(database, function() {
    console.log('Create a new database: ', database)
    // Loop over the data directory & get list of all caseIds
    fs.readdir(p, function (err, dirs) {
      if (err) {
        throw err;
      }
      dirs.map(function (dir) {
        return path.join(p, dir);
      }).filter(function (dir) {
        return fs.statSync(dir).isDirectory();
      // Loop over a caseId directory
      }).forEach(function (dir) {
          fs.readdir(dir, function (err, files) {
            if (err) {
              throw err;
            }
            files.map(function (f) {
              return path.join(dir, f);
            }).filter(function (f) {
              return fs.statSync(f).isFile();
            }).forEach(function (f) {
              indexDocument(f)
            });
          });
      });
    });
  });
});
