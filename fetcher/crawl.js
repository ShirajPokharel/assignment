// Helper Modules
var utils = require('./utils');
var download = require('./download');

// Module for JavaScript Based Crawling
var Nightmare = require('nightmare');

// Module for Exponential Backoff
var Backoff = require('backo');
var backoff = new Backoff({ min: 1000, max: 10000 });

var nightmare;

// Portal Base URL
var url = 'https://publicrecordsaccess.fultoncountyga.gov/Portal/'

// Prefix to be attached to caseId
// var prefix = ['15DE000', '15ED000', '16DE000', '16ED000']
var prefix = ['15DE']

// Padding Bit
var paddingBits = 6

// Starting CaseNum
var caseNum = 1

// Ending CaseNum
var finalCaseNum = 1000


// Function to download documents
var downloadDocument = function (url, dest) {
  nightmare = Nightmare({ show: true })
  nightmare
    .goto(url)
    .evaluate(function () {
      downloadLink = document.querySelector('#main #content div.portlet-container-wrapper.tyler-rounded-5 #div_zone1pi0 div.portlet-body div.portlet-footer a').href
      return downloadLink
    })
    .end()
    .then(function (link) {
      download.download(link, dest, function(err, dest){
        if(err) {
          throw err;
        }
        console.log('Download Complete: %s', dest)
      })
    })
}

var runNext = function (url, prefix, caseNum, finalCaseNum, downloadDocs) {
  for(prefixIndex = 0; prefixIndex < prefix.length; prefixIndex++) {
    if(caseNum < finalCaseNum) {
      caseString = prefix[prefixIndex] + utils.pad(caseNum, paddingBits)
      destDir = __dirname + '/../data/' + caseString + '/documents'
      utils.createDir(destDir)
      nightmare = Nightmare({ show: true })
      nightmare
        .goto(url)
        .type('input[id="caseCriteria_SearchCriteria"]', caseString)
        .select('input[id="caseCriteria_CourtLocation"]', 'Magistrate Court')
        .click('input[id="btnSSSubmit"]')
        .wait('#main #tab-control #ui-tabs-1 div.portlet-body div.portlet-container div.expand-collapse-group section #CasesContainer #CasesGrid table tbody tr td a')
        .click('a[class="caseLink"]')
        .wait('#main #tab-control #tcBody_2 div.portlet-body form #caseInformationDiv #divCaseInformation_body div')
        .evaluate(function () {
          returnObject = {}

          // Case Information Extraction
          if (document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #caseInformationDiv')) {
            for(i = 0, caseInformation = {}; i < document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #caseInformationDiv #divCaseInformation_body div').querySelectorAll('p').length; i++) {
              key = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #caseInformationDiv #divCaseInformation_body div').querySelectorAll('p')[i].innerText.split(':')[0].trim()
              value = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #caseInformationDiv #divCaseInformation_body div').querySelectorAll('p')[i].innerText.split(':')[1].trim()
              caseInformation[key] = value
            }
            returnObject['caseInformation'] = caseInformation
          }

          // Party Information Extraction
          if (document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv')) {
            for(i = 0, partyInformation = {}; i < document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body').querySelectorAll('div.tyler-cols-3').length; i++) {
              if (document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body').querySelectorAll('div.tyler-cols-3')[i].querySelector('div.tyler-span-2')) {
                if (document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body').querySelectorAll('div.tyler-cols-3')[i].querySelectorAll('div.tyler-span-2').length == 1) {
                  for(j = 0, partyInformationItem = {}; j < document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body').querySelectorAll('div.tyler-cols-3')[i].querySelector('div.tyler-span-2').querySelectorAll('p').length; j++) {
                    key = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body div.tyler-cols-3 div.tyler-span-2').querySelectorAll('p')[j].innerText.split(':')[0]
                    value = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body div.tyler-cols-3 div.tyler-span-2').querySelectorAll('p')[j].innerText.split(':')[1]
                    partyInformationItem[key] = value
                    if (key == "Address") {
                      if (document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body div.tyler-cols-3 div.tyler-span-2')) {
                        partyInformationItem[key] = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body div.tyler-cols-3 div.tyler-span-2').querySelectorAll('div')[1].querySelector('p').innerText
                      } else {
                        partyInformationItem[key] = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body div.tyler-cols-3 div.tyler-span-3').querySelectorAll('div')[1].querySelector('p').innerText
                      }
                    }
                  }
                }
              }
              else if (document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body').querySelectorAll('div.tyler-cols-3')[i].querySelector('div.tyler-span-3')) {
                if (document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body').querySelectorAll('div.tyler-cols-3')[i].querySelectorAll('div.tyler-span-3').length == 1) {
                  for(j = 0, partyInformationItem = {}; j < document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body').querySelectorAll('div.tyler-cols-3')[i].querySelectorAll('div.tyler-span-3')[0].querySelectorAll('p').length; j++) {
                    key = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body').querySelectorAll('div.tyler-cols-3')[i].querySelectorAll('div.tyler-span-3')[0].querySelectorAll('p')[j].innerText.split(':')[0]
                    value = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body').querySelectorAll('div.tyler-cols-3')[i].querySelectorAll('div.tyler-span-3')[0].querySelectorAll('p')[j].innerText.split(':')[1]
                    partyInformationItem[key] = value
                    if (key == "Address") {
                      if (document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body div.tyler-cols-3 div.tyler-span-2')) {
                        partyInformationItem[key] = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body div.tyler-cols-3 div.tyler-span-2').querySelectorAll('div')[1].querySelector('p').innerText
                      } else {
                        partyInformationItem[key] = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body div.tyler-cols-3 div.tyler-span-3').querySelectorAll('div')[1].querySelector('p').innerText
                      }
                    }
                  }
                }
                else {
                  for(j = 0, partyInformationItem = {}; j < document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body').querySelectorAll('div.tyler-cols-3')[i].querySelectorAll('div.tyler-span-3')[1].querySelectorAll('p').length; j++) {
                    key = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body').querySelectorAll('div.tyler-cols-3')[i].querySelectorAll('div.tyler-span-3')[1].querySelectorAll('p')[j].innerText.split(':')[0]
                    value = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body').querySelectorAll('div.tyler-cols-3')[i].querySelectorAll('div.tyler-span-3')[1].querySelectorAll('p')[j].innerText.split(':')[1]
                    partyInformationItem[key] = value
                    if (key == "Address") {
                      if (document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body div.tyler-cols-3 div.tyler-span-3')) {
                        partyInformationItem[key] = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body').querySelectorAll('div.tyler-cols-3')[i].querySelectorAll('div.tyler-span-3')[1].querySelectorAll('div')[1].querySelector('p').innerText
                      } else {
                        partyInformationItem[key] = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #partyInformationDiv #divPartyInformation_body').querySelectorAll('div.tyler-cols-3')[i].querySelectorAll('div.tyler-span-2')[1].querySelectorAll('div')[1].querySelector('p').innerText
                      }
                    }
                  }
                }
              }
              else {

              }
              partyInformation[i] = partyInformationItem
            }
            returnObject['partyInformation'] = partyInformation
          }

          // Events Information Extraction
          if (document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #eventsInformationDiv')) {
            for(i = 0, eventsInformation = {}; i < document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #eventsInformationDiv #divEventsInformation_body').querySelectorAll(':scope > div').length; i++) {
                key = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #eventsInformationDiv #divEventsInformation_body').querySelectorAll(':scope > div')[i].querySelector('div.tyler-toggle-controller').innerText
                if (document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #eventsInformationDiv #divEventsInformation_body').querySelectorAll(':scope > div')[i].querySelector('div.tyler-toggle-container p span a')) {
                  eventsInformationItem = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #eventsInformationDiv #divEventsInformation_body').querySelectorAll(':scope > div')[i].querySelector('div.tyler-toggle-container p span a').href
                } else {
                  eventsInformationItem = ""
                }
                eventsInformation[key] = eventsInformationItem
            }
            returnObject['eventsInformation'] = eventsInformation
          }

          // Financial Information Extraction
          // financialInformation = {}
          // if (document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #financialInformationDiv')) {
          //   for(i = 0; i < document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #financialInformationDiv #divFinancialInformation_body #financialSlider div.k-widget.k-grid table tbody').querySelectorAll('tr').length; i++) {
          //     for(j = 0, value = ""; j < document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #financialInformationDiv #divFinancialInformation_body #financialSlider div.k-widget.k-grid table tbody').querySelectorAll('tr')[i].querySelectorAll('td').length; j++) {
          //       value = value + " " + document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #financialInformationDiv #divFinancialInformation_body #financialSlider div.k-widget.k-grid table tbody').querySelectorAll('tr')[i].querySelectorAll('td')[j].innerText
          //     }
          //     financialInformation[i] = value
          //   }
          //   returnObject['financialInformation'] = financialInformation
          // }

          // Documents Extraction
          if (document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #documentsInformationDiv')) {
            for(i = 0, documentsInformation = {}; i < document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #documentsInformationDiv #divDocumentsInformation_body').querySelectorAll(':scope > p').length; i++) {
              key = ""
              value = ""
              if (document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #documentsInformationDiv #divDocumentsInformation_body p')) {
                key = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #documentsInformationDiv #divDocumentsInformation_body').querySelectorAll(':scope > p')[i].querySelector('span').innerText.replace(/\n|\r/g, "").trim()
                value = document.querySelector('#main #tab-control #tcBody_2 div.portlet-body form #documentsInformationDiv #divDocumentsInformation_body').querySelectorAll(':scope > p')[i].querySelector('span a').href
              }
              documentsInformation[key] = value
            }
            returnObject['documentsInformation'] = documentsInformation
          }

          return returnObject
        })
        .end()
        .then(function (output) {
          // Flush to stdout
          console.log(output)

          // Flush to disk
          utils.writeFile(__dirname + '/../data/' + caseString + '/caseInformation', output['caseInformation'])
          utils.writeFile(__dirname + '/../data/' + caseString + '/partyInformation', output['partyInformation'])
          utils.writeFile(__dirname + '/../data/' + caseString + '/eventsInformation', output['eventsInformation'])
          utils.writeFile(__dirname + '/../data/' + caseString + '/financialInformation', output['financialInformation'])
          utils.writeFile(__dirname + '/../data/' + caseString + '/documentsInformation', output['documentsInformation'])

          // Download Documents
          if (downloadDocs == true) {
            for (var key in output['documentsInformation']) {
              destFile = destDir + "/" + key
              downloadDocument(output['documentsInformation'][key], destFile)
            }
          }

          // Crawl next case
          runNext(url, prefix, caseNum+1, finalCaseNum, downloadDocs)
        })
      }
  }
}

// Function to check whether the portal is up/down
var checkPortal = function (url, caseString) {
  nightmare = Nightmare({ show: true })
  nightmare
    .goto(url)
    .type('input[id="caseCriteria_SearchCriteria"]', caseString)
    .select('input[id="caseCriteria_CourtLocation"]', 'Magistrate Court')
    .click('input[id="btnSSSubmit"]')
    .wait('#main #tab-control #ui-tabs-1 div.portlet-body div.portlet-container div.expand-collapse-group section #CasesContainer #CasesGrid table tbody tr td a')
    .exists('a[class="caseLink"]')
    .end()
    .then(function (result) {
      console.log(result)
    });
}

// Exponential backoff function
// setTimeout(function(){
//   checkPortal(url, '16ED000001');
// }, backoff.duration());

// Checks whether to download the attached documents
if (process.argv.length == 2) {
  runNext(url, prefix, caseNum, finalCaseNum, false)
} else {
  runNext(url, prefix, caseNum, finalCaseNum, true)
}

// backoff.reset()
