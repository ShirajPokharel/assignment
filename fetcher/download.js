var request = require('request');
var fs = require('fs');
var path = require('path');
var Promise = require('bluebird');

module.exports = {

  // Helper function to download a file from its URL
  download: function (url, dest, callback) {
    var p = new Promise(function(resolve, reject){
      var writeStream = fs.createWriteStream(dest);

      writeStream.on('finish', function(){
        resolve(dest);
      });

      writeStream.on('error', function(err){
        fs.unlink(dest, reject.bind(null, err));
      });

      var readStream = request.get(url);

      readStream.on('error', function(err){
        fs.unlink(dest, reject.bind(null, err));
      });

      readStream.pipe(writeStream);
    });

    if(!callback)
      return p;

    p.then(function(dest){
      callback(null, dest);
    }).catch(function(err){
      callback(err);
    });
  },
};
